# Zadanie 3 – z różnymi pętlami i tablicą jednowymiarową (0.75 pkt)

Napisać program, który:

1. wczytuje napisy aż do wczytania napisu kończącego się kropką i drukuje informację, o ile najdłuższy wczytany napis jest dłuższy od najkrótszego. Nie należy brać pod uwagę ostatniego napisu (tego z kropką) kończącego ciąg. Należy zastosować pętlę while.
1. potem wczytuje liczbę całkowitą k, wymuszając, by k było dodatnie i mniejsze od stałej g (pętlą do-while).
1. następnie wczytuje ciąg k napisów (pętlą for), po czym drukuje łączną ilość znaków będących cyframi w tych wszystkich napisach.
1. na koniec losuje n dużych liter (n – stała) do tablicy Z\[n\], drukuje tę tablicę, zamienia dwa ostatnie znaki z dwoma pierwszymi i ponownie drukuje tablicę (to wszystko pętlą for).